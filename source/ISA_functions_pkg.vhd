-- Copyright 2014 Vincent Camus all rights reserved
--
-- EPFL, ICLAB     http://iclab.epfl.ch/approximate
-- Vincent Camus   vincent.camus@epfl.ch
--
-- Friday, May 16 2014
-- Version 1.0


------------------------------------------------------------------- PACKAGE DECLARATION

PACKAGE functions_pkg IS

	FUNCTION char_to_int (char : CHARACTER) RETURN INTEGER;
	FUNCTION block_start (REG : STRING; n : INTEGER) RETURN INTEGER;

END PACKAGE functions_pkg;

------------------------------------------------------------------- PACKAGE DESCRIPTION

PACKAGE BODY functions_pkg IS

	-- useful function
	FUNCTION char_to_int (char : CHARACTER)
		RETURN INTEGER IS
	BEGIN
		CASE char IS
			WHEN '0' => RETURN 0;
			WHEN '1' => RETURN 1;
			WHEN '2' => RETURN 2;
			WHEN '3' => RETURN 3;
			WHEN '4' => RETURN 4;
			WHEN '5' => RETURN 5;
			WHEN '6' => RETURN 6;
			WHEN '7' => RETURN 7;
			WHEN '8' => RETURN 8;
			WHEN '9' => RETURN 9;
			WHEN 'a' => RETURN 10;
			WHEN 'b' => RETURN 11;
			WHEN 'c' => RETURN 12;
			WHEN 'd' => RETURN 13;
			WHEN 'e' => RETURN 14;
			WHEN 'f' => RETURN 15;
			WHEN 'g' => RETURN 16;
			WHEN 'h' => RETURN 17;
			WHEN 'i' => RETURN 18;
			WHEN 'j' => RETURN 19;
			WHEN 'k' => RETURN 20;
			WHEN 'l' => RETURN 21;
			WHEN 'm' => RETURN 22;
			WHEN 'n' => RETURN 23;
			WHEN 'o' => RETURN 24;
			WHEN 'p' => RETURN 25;
			WHEN 'q' => RETURN 26;
			WHEN 'r' => RETURN 27;
			WHEN 's' => RETURN 28;
			WHEN 't' => RETURN 29;
			WHEN 'u' => RETURN 30;
			WHEN 'v' => RETURN 31;
			WHEN 'w' => RETURN 32;
			WHEN 'x' => RETURN 33;
			WHEN 'y' => RETURN 34;
			WHEN 'z' => RETURN 35;
			WHEN 'A' => RETURN 36;
			WHEN 'B' => RETURN 37;
			WHEN 'C' => RETURN 38;
			WHEN 'D' => RETURN 39;
			WHEN 'E' => RETURN 40;
			WHEN 'F' => RETURN 41;
			WHEN 'G' => RETURN 42;
			WHEN 'H' => RETURN 43;
			WHEN 'I' => RETURN 44;
			WHEN 'J' => RETURN 45;
			WHEN 'K' => RETURN 46;
			WHEN 'L' => RETURN 47;
			WHEN 'M' => RETURN 48;
			WHEN 'N' => RETURN 49;
			WHEN 'O' => RETURN 50;
			WHEN 'P' => RETURN 51;
			WHEN 'Q' => RETURN 52;
			WHEN 'R' => RETURN 53;
			WHEN 'S' => RETURN 54;
			WHEN 'T' => RETURN 55;
			WHEN 'U' => RETURN 56;
			WHEN 'V' => RETURN 57;
			WHEN 'W' => RETURN 58;
			WHEN 'X' => RETURN 59;
			WHEN 'Y' => RETURN 60;
---------------------------------------------- !!!
			WHEN 'Z' => RETURN 64;
---------------------------------------------- !!!
			WHEN OTHERS => ASSERT (FALSE) REPORT "No char character read" SEVERITY FAILURE;
		END CASE;
	END FUNCTION char_to_int;

	-- useful function
	FUNCTION block_start (REG : STRING; n : INTEGER)
		RETURN INTEGER IS
		VARIABLE x : INTEGER;
	BEGIN
		x := 0;
		FOR i IN 1 TO n LOOP
			x := x + char_to_int(REG(i));
		END LOOP;
		RETURN x;
	END FUNCTION block_start;

END PACKAGE BODY functions_pkg;
