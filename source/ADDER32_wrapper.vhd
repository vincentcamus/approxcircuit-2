-- Copyright 2014 Vincent Camus all rights reserved
--
-- EPFL, ICLAB     http://iclab.epfl.ch/approximate
-- Vincent Camus   vincent.camus(at)epfl.ch
--
-- Friday, May 16 2014
-- Version 1.0


------------------------------------------------------------------- TOP ENTITY DECLARATION

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;


ENTITY ADDER32 IS
	PORT (
		in_c	: IN    STD_LOGIC;
		in_a    : IN    STD_LOGIC_VECTOR (31 DOWNTO 0);
		in_b    : IN    STD_LOGIC_VECTOR (31 DOWNTO 0);
		out_s   : OUT   STD_LOGIC_VECTOR (32 DOWNTO 0));
END ENTITY ADDER32;


------------------------------------------------------------------- TOP ENTITY DESCRIPTION

ARCHITECTURE RTL OF ADDER32 IS

---------------------------------- PARAMETERS

	-- modify ISA parameters here
	CONSTANT ADDER_WIDTH : INTEGER := 32;
	CONSTANT BLOCK_NB    : INTEGER := 8;
	CONSTANT ADD_REG     : STRING  := "45643244"; -- ADD sizes       0-9,a-z,A-Y and Z=64 !!!
	CONSTANT SPEC_REG    : STRING  := "-1232100"; -- SPEC sizes      0-9,a-z,A-Y
	CONSTANT GUESS_REG   : STRING  := "-01ABPX-"; -- SPEC guesses    0,1,A,B,G,P,X,C,-
	CONSTANT TEST_REG    : STRING  := "-SSSSSSS"; -- COMP types      S,N
	CONSTANT COR_REG     : STRING  := "-1230000"; -- COMP cor sizes  0-9,a-z,A-Y
	CONSTANT RED_REG     : STRING  := "-0032100"; -- COMP red sizes  0-9,a-z,A-Y

---------------------------------- COMPONENTS
	
	-- component declaration
	COMPONENT ISA IS
	GENERIC (
		ADDER_WIDTH : INTEGER; -- total bit-width of the ISA
		BLOCK_NB    : INTEGER; -- number of blocks of the ISA
		ADD_REG     : STRING;  -- ADD sizes                      char sizes: 0-9,a-z,A-Y=0..60 and Z=64 !!!
		SPEC_REG    : STRING;  -- SPEC sizes                     char sizes: 0-9,a-z,A-Y=0..60
		GUESS_REG   : STRING;  -- SPEC guesses                   char types: 0,1,A,B,G,P,X,C,-
		TEST_REG    : STRING;  -- COMP types                     char types: S=standard,N=no-test
		COR_REG     : STRING;  -- COMP correction sizes          char sizes: 0-9,a-z,A-Y=0..60
		RED_REG     : STRING); -- COMP reduction/balancing sizes char sizes: 0-9,a-z,A-Y=0..60
	PORT (
		a, b   : IN  STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		cin    : IN  STD_LOGIC;
		s      : OUT STD_LOGIC_VECTOR (ADDER_WIDTH-1 DOWNTO 0);
		cout   : OUT STD_LOGIC);
	END COMPONENT ISA;

---------------------------------- DESCRIPTION
	
BEGIN
	
	-- component instanciation
	inst_ISA: ISA
	GENERIC MAP ( ADDER_WIDTH, BLOCK_NB, ADD_REG, SPEC_REG, GUESS_REG, TEST_REG, COR_REG, RED_REG )
	PORT MAP ( in_a(ADDER_WIDTH-1 DOWNTO 0), in_b(ADDER_WIDTH-1 DOWNTO 0), in_c, out_s(ADDER_WIDTH-1 DOWNTO 0), out_s(ADDER_WIDTH) );

END ARCHITECTURE RTL;

