#Function: run ISA gate-level simulation 
#clock period: how much clock period we want to run the netlist 
#pre-requisite: 
#1) ADD ps TO the post-synthesis module 

use File::Copy;
@clock_list = ("0.3");

################## GET ALL NETLIST NAME ######################################
my @dname_list = qw();
my $dir = '../../../netlist';
foreach my $fp (glob("$dir/*.v")) {
  my $dname = substr $fp, 17, -2;
  push (@dname_list, $dname)
}

################## COMPILE TESTBENCH FILES #################################
system("vlog -quiet -93 -work work ../testbench/tb_comb.v");

################## EXECUTE DIFFERENT NETLIST AND CLK #############################
foreach my $dname (@dname_list)
{
    system("vlog  -quiet -93 -work work ../../../netlist/$dname.v");
    foreach $clock_period (@clock_list)
    {
        system("rm -rf configure.txt");
        system("echo $clock_period >> configure.txt");
        #system("vsim -c work.tb -do \"set StdArithNoWarnings 1; run -all\"");
        system("vsim -noglitch -c -sdfnoerror -sdfnowarn +no_tchk_msg -sdfmax tb/UUT2=../../../netlist_SDF/$dname.sdf work.tb -do \"set StdArithNoWarnings 1; run -all\"");
        system("mv ../REP/diamond.csv ../REP/diamond.csv");
        system("mv ../REP/golden.csv ../REP/golden.$dname.csv");
        system("mv ../REP/silver5.csv ../REP/silver.$dname.0.285.csv");
        system("mv ../REP/silver10.csv ../REP/silver.$dname.0.27.csv");
        system("mv ../REP/silver15.csv ../REP/silver.$dname.0.255.csv");
    }
}

################### TRASH CODE ###############################
# simulate with GUI 
#system("vsim work.tb");
#system("vsim -c work.tb -do \" set StdArithNoWarnings 1; run -all\"");

