//Function: for combinational circuit  
//Input: post-syn netlist, SDF  
//Output: diamond, golden, silver output
//Prerequisite: change the UUT name, change the port map in UUT declaration
//and top module declaration, and correspoding operand reading config. 
`timescale 1ns/1ps

module tb(output reg [31:0] X,
          output reg [31:0] Y,
          output [32:0] R_diamond,
          output [32:0] R_golden,
          output [32:0] R_silver);
   
    
    reg [63:0] FF_error_cnt;
    integer N, L, M, K, test1, test2;
    parameter L4 = 32;
    integer line_number, comma_1, comma_2; 
    reg Cin = 0;
    reg Cin_in;
    integer diamond_file, golden_file, silver5_file,silver10_file,silver15_file; 
    integer in_file, config_file, statusI, c_out;
    wire [31:0] c1, c2;
    reg [31:0] X_in, Y_in;
    //reg [32:0] R_diamond; 
    real  input_delay, capture_delay, half_period, clk_period;
    real incr_delay, capture_delay5, capture_delay10, capture_delay15;
    

    assign R_diamond = X + Y + Cin; 
    ADDER32 UUT1(.in_a(X), .in_b(Y), .in_c(Cin), .out_s(R_golden));
    ADDER32 UUT2(.in_a(X), .in_b(Y), .in_c(Cin), .out_s(R_silver));
 
    initial
    begin       
       diamond_file = $fopen("../REP/diamond.csv", "w");
       golden_file = $fopen("../REP/golden.csv", "w");
       silver5_file = $fopen("../REP/silver5.csv", "w");
       silver10_file = $fopen("../REP/silver10.csv", "w");
       silver15_file = $fopen("../REP/silver15.csv", "w");
       config_file = $fopen("../sim/configure.txt", "r");
       statusI = $fscanf(config_file,"%f",clk_period);
       //half_period=0.5*clk_period;
       $display("clock period is: %f", clk_period);
       input_delay=2*clk_period;
       //capture_delay5=clk_period*0.95;
       //capture_delay10=clk_period*0.90;
       capture_delay15 = clk_period*0.85;
       incr_delay = clk_period*0.05;
     end
     
    initial 
     begin
       M=32;
       in_file  = $fopen("../stimuli_data/stimuli_1m.csv", "r");
       //in_file  = $fopen("../../../stimuli_data/rand_unif_u32_high.csv", "r");
       test1=0;
       test2=0;
    end  

    initial
     begin
         forever  
         begin
                  #input_delay  
                  //$display("%d\n", in_file);
                  test1 = $fscanf(in_file, "%d%c%b%c%b\n", line_number, comma_1, X, comma_2, Y);
                  if(test1 !== 5)
                  begin
                      $fclose(in_file);
                      $finish;
                  end
		  /************ RECORD DIAMOND, GOLDEN AND SILVER DATA **************/
		  #capture_delay15
	          $fdisplay(diamond_file, "%b", R_diamond); 
	          $fdisplay(golden_file, "%b", R_golden); 
	          $fdisplay(silver15_file, "%b", R_silver); 
		  /************ RECORD DIAMOND, GOLDEN AND SILVER DATA **************/
		  #incr_delay
		  $fdisplay(silver10_file, "%b", R_silver); 
		  /************ RECORD DIAMOND, GOLDEN AND SILVER DATA **************/
		  #incr_delay
		  $fdisplay(silver5_file, "%b", R_silver); 
         end
     end
endmodule

/****************** TRASH CODE ***************************/
/*
$fwrite(outport_file, "x");
$fwrite(outport_file, "\n"); 

    always @(X or Y or Cin)
    begin 
      #capture_delay5
      $fdisplay(diamond_file, "%b", R_diamond); 
      $fdisplay(golden_file, "%b", R_golden); 
      $fdisplay(silver5_file, "%b", R_silver); 
      #incr_delay
      $fdisplay(silver10_file, "%b", R_silver); 
      #incr_delay
      $fdisplay(silver15_file, "%b", R_silver); 
    end      
*/

