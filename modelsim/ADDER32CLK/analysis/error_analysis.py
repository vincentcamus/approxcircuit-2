#Function:
# 1) display erroneous cycle in golden and silver 
# 2) generate bit-level golden error file and silver error file 

from collections import defaultdict

#return erroneous cycles 
def compute_err_cycle(a_list, b_list):
   err_cycle = 0 
   for a, b in zip(a_list, b_list):
       if a != b: 
           err_cycle += 1 
   return err_cycle 

#return bit level error count
def compute_bit_err(a_list, b_list):
   bit_err = defaultdict(int)
   bit_len = len(a_list[0].rstrip())
   for a, b in zip(a_list, b_list):
       for i in range(bit_len):
           bit_err[bit_len-i] += (a[i] != b[i])
   return bit_err      

diamond_file = open("../REP/diamond.csv")
golden_file = open("../REP/golden.csv")
silver_file = open("../REP/silver.csv")

diamond_list = diamond_file.readlines()
golden_list = golden_file.readlines()
silver_list = silver_file.readlines()

print compute_err_cycle(diamond_list, golden_list)
print compute_err_cycle(diamond_list, silver_list)
print compute_bit_err(diamond_list, golden_list)
print compute_bit_err(diamond_list, silver_list)
 
